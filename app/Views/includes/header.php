<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><?= $page_name ?> : Manage Movie Trailers</title>
	<link  rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.min.css');?>">
	<link  rel="stylesheet" type="text/css" href="<?= base_url('assets/css/main.css');?>">
	<link  rel="stylesheet" type="text/css" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css');?>">
		
  </head>
  <body>