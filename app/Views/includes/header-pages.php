
<header class="p-3 mb-3 border-bottom">
  <div class="container">
    <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
      <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
        <li><a href="<?= base_url('Dashboard') ?>" class="text-uppercase fs-4 fw-bolder text-decoration-none">Home</a></li>
      </ul>
      <div class="dropdown text-end">
        <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
		<?php  $local_session = \Config\Services::session(); ?>
          Welcome <?= ($local_session->get('name')) ?>
        </a>
        <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
          <li><a class="dropdown-item" href="<?= base_url('User') ?>"><i class="fa fa-cog"></i> My Account</a></li>
          <li><hr class="dropdown-divider"></li>
          <li><a class="dropdown-item" href="<?= base_url('Login/Logout') ?>"><i class="fa fa-sign-out"></i> Sign out</a></li>
        </ul>
      </div>
    </div>
  </div>
</header>