
  <body class="text-center">
	<div class="signin-form">
	<div class="form-signin">
		<?php if (isset($validation)) : ?>
			<div class="col-12">
				<div class="alert alert-danger" role="alert">
					<?= $validation->listErrors() ?>
				</div>
			</div>
		<?php endif; ?>
	    <?php if(session()->getFlashdata('msg')):?>
			<div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
		<?php endif;?>
		<form action="<?= base_url('Login') ?>" method="post">
		<h1 class="h3 mb-3 fw-normal"><?= lang("app.please")." ".lang("app.sign_in") ?></h1>
			<a class="dropdown-item" href="<?= site_url('lang/en'); ?>">English</a>
			<a class="dropdown-item" href="<?= site_url('lang/fr'); ?>">Kiswahili</a>
		<div class="mb-3"></div>
		<div class="form-floating">
		  <input type="email" class="form-control" name="email" placeholder="name@example.com" required>
		  <label><?= lang("App.email_address")?></label>
		</div>
		<div class="form-floating">
		  <input type="password" class="form-control" name="password" placeholder="Password" required>
		  <label ><?= lang("App.password")?></label>
		</div>
		<button class="w-100 btn btn-lg btn-primary" type="submit"><?= lang("app.sign_in")?></button>
	  </form>
	</div>
	</div>
  </body>
