
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">View Movie Trailer</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
		<?php if($trailerData) {
			$trailer = $trailerData[0];	?>
        <div class="mb-3">
		  <label class="form-label">Trailer Name</label>
		  <input type="text" class="form-control" placeholder="Enter Trailer Name" disabled readonly value="<?= $trailer['name'] ?>">
		</div>
		<div class="mb-3">
			<div class="embed-responsive embed-responsive-21by9">
			  <iframe class="embed-responsive-item" src="<?= $trailer['url'] ?>" allowfullscreen></iframe>
			</div>
		</div>
		<?php } else { ?>
		<div class="alert alert-danger" role="alert">
		  Sorry the requested content can not be found!!!
		</div>
		<?php } ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>