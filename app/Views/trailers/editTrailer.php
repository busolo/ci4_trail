	<form action="#" id="trailerFormEdit" class="form-horizontal">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Edit Movie Trailer</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
		<?php if($trailerData) {
			$trailer = $trailerData[0];	?>
        <div class="mb-3">
		  <label class="form-label">Trailer Name</label>
		  <input type="hidden" name="trailerID" class="form-control" placeholder="Enter Trailer Name" required value="<?= $trailer['trailer_id'] ?>">
		  <input type="text" name="name"  class="form-control" placeholder="Enter Trailer Name" required value="<?= $trailer['name'] ?>">
		  <input type="hidden" id="url" value="<?php echo base_url('Dashboard/editTrailer')?>">
		  <input type="hidden" name="type" value="Edit">
		</div>
        <div class="mb-3">
		  <label class="form-label">Trailer URL</label>
		  <input type="text" name="url"  class="form-control" placeholder="Enter Trailer URL" required value="<?= $trailer['url'] ?>">
		</div>
        <div class="mb-3">
		  <label class="form-label">Order Number</label>
		  <input type="text" name="order"  class="form-control" placeholder="Enter Trailer Order Number" required value="<?= $trailer['order'] ?>">
		</div>
		<?php } else { ?>
		<div class="p-5">
		<div class="alert alert-danger" role="alert">
		  Sorry the requested content can not be found!!!
		</div>
		</div>
		<?php } ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update Trailer</button>
      </div>
	  </form>
	  
	  
	<script>
	$(document).ready(function() {
		$('#trailerFormEdit').submit(function(e) {
			e.preventDefault();
			var site_url = $("#url").val();
			$.ajax({
				url : site_url,
				type: "POST",
				data: $('#trailerFormEdit').serialize(),
				dataType: "JSON",
				success: function(data) {
					//Get Results
					//Check success
					if(data.success == "success") {
						$('#editMovie').modal('hide');
						alert(data.msg);		
						setTimeout(function(){// wait for 5 secs(2)
							location.reload(); // then reload the page.(3)
						}, 1000); 
					   //setTimeout(location.reload.bind(location), 10000);				   
					} else {
					   alert(data.msg);					
					}
			   },
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Error submitting your data, please try again');
				}
		   });
		});
	});
	</script>