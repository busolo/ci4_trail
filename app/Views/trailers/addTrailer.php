
    <div class="modal-content">
	<form action="#" id="addTrailerForm" class="form-horizontal">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Add Movie Trailer</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="mb-3">
			<label class="form-label">Trailer Name</label>
			<input type="text" name="name" class="form-control" placeholder="Enter Trailer Name" required>
			<input type="hidden" id="url" value="<?php echo base_url('Dashboard/editTrailer')?>">
			<input type="hidden" name="type" value="Add">
		</div>
		<div class="mb-3">
		  <label class="form-label">Trailer URL</label>
		  <input type="text" name="url" class="form-control" placeholder="Enter Trailer URL" required>
		</div>
		<div class="mb-3">
		  <label class="form-label">Trailer Order</label>
		  <input type="text" name="order" class="form-control" placeholder="Enter Trailer Order Number" required>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Add Trailer</button>
      </div>
	  </form>
    </div>
	
	<script>
	$(document).ready(function() {
		$('#addTrailerForm').submit(function(e) {
			e.preventDefault();
			var site_url = $("#url").val();
			$.ajax({
				url : site_url,
				type: "POST",
				data: $('#addTrailerForm').serialize(),
				dataType: "JSON",
				success: function(data) {
					//Get Results
					//Check success
					if(data.success == "success") {
						$('#addTrailerForm')[0].reset();
						$('#editMovie').modal('hide');
						alert(data.msg);
						setTimeout(function(){// wait for 5 secs(2)
							location.reload(); // then reload the page.(3)
						}, 1000);					   
					} else {
					   alert(data.msg);					
					}
			   },
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Error submitting your data, please try again');
				}
		   });
		});
	});
	</script>