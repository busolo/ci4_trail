
<div class="container" id="main" data-page="dashboard">
	<div class="col-md-12 text-end">
      <button type="button" class="btn btn-success" onclick="view(0, 'Add', '<?php echo base_url('Dashboard/addTrailer')?>')"><i class="fa fa-plus"></i> Add a Movie Trailer</button>
    </div>

  <div class="my-3 p-3 bg-body rounded">
    <h6 class="border-bottom pb-2 mb-0">List of Movie Trailers</h6>
    <div id="userTable">
	  <table class="table table-hover table-striped">
		<thead>
		  <tr>
			<th>Trailer ID</th>
			<th>Name</th>
			<th>URL</th>
			<th>Actions</th>
		  </tr>
		</thead>
		<tbody>
		  <?php foreach($trailers as $trailer) {  ?>
		  <tr>
			<td><?php echo $trailer['trailer_id']; ?></td>
			<td><?php echo $trailer['name']; ?></td>
			<td><?php echo $trailer['url']; ?></td>
			<td>
				<button type="button" class="btn btn-primary btn-sm" onclick="view(<?php echo $trailer['trailer_id'];?>, 'View', '<?php echo base_url('Dashboard/addTrailer')?>')"><i class="fa fa-eye"></i> view</button>
				<button type="button" class="btn btn-info btn-sm" onclick="view(<?php echo $trailer['trailer_id'];?>, 'Edit', '<?php echo base_url('Dashboard/addTrailer')?>')"><i class="fa fa-pencil"></i> edit</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="view(<?php echo $trailer['trailer_id'];?>, 'Delete', '<?php echo base_url('Dashboard/addTrailer')?>')"><i class="fa fa-trash"></i> delete</button>
			</td>
		  </tr>
		  <?php } ?>
		</tbody>
	  </table>
	
	</div>

  </div>
</div>
<!-- Add Movie Modal -->
<div class="modal fade" id="addMovie" aria-hidden="true">
  <div class="modal-dialog" id="add-trailer-div">
  </div>
</div>

<!-- Actions -- Edit, Delete and View -->
<div class="modal fade" id="editMovie" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-content">
	
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
		$('form').submit(function(e) {
        e.preventDefault();
		var site_url = $("#url").val();
        $.ajax({
			url : site_url,
            type: "POST",
            data: $('#trailerForm').serialize(),
            dataType: "JSON",
            success: function(data) {
				//Get Results
				//Check success
				if(data.success == "success") {
				   $('#trailerForm')[0].reset();
				   $('#editMovie').modal('hide');
				   $('#addMovie').modal('hide');
				   alert(data.msg);		
				   //setTimeout(location.reload.bind(location), 10000);				   
				} else {
				   alert(data.msg);					
				}
           },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error submitting your data, please try again');
            }
       });
     });
});

	
		function view(id, type, site_url) {
		  //$('#formView')[0].reset(); // reset form on modals
		  //Ajax Load data from ajax
			  $.ajax({
				url : site_url,
				type: "POST",
				data: {type:type, trailer_id: id},
				dataType: "HTML",
				success: function(data) {
					if(type == "Add") {
						$('#editMovie').modal('show'); // show bootstrap modal when complete loaded
						$('.modal-title').html(type + " Movie Trailer"); // Set title to Bootstrap modal title
						//Add data to Modal
						$('.modal-content').html(data);
					} else if(type == "Delete") {
						alert(data);		
						setTimeout(function(){// wait for 5 secs(2)
							location.reload(); // then reload the page.(3)
						}, 1000); 
					} else {
						$('#editMovie').modal('show'); // show bootstrap modal when complete loaded
						$('.modal-title').html(type + " Movie Trailer"); // Set title to Bootstrap modal title
						//Add data to Modal
						$('.modal-content').html(data);
						//$('.add-trailer-div').html(data);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
					alert('Error get data from ajax');
				}
			});
		}
</script>