
<main class="container">
	<div class="card">
	  <div class="card-header">
		<div class="row">
			<div class="col-md-6">
				<h5 class="card-title text-success"><?= $page_name ?></h5>
			</div>
			<div class="col-md-6">
				<button type="button" class="btn btn-primary pull-right" data-bs-toggle="modal" data-bs-target="#changePassword"> Change Password</button>
			</div>
		</div>
		
	  </div>
	  <div class="card-body">
		<form>
		  <div class="row mb-3">
			<label class="col-sm-2 col-form-label">Name</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" disabled readonly value="<?= $user['name'] ?>">
			</div>
		  </div>
		  <div class="row mb-3">
			<label class="col-sm-2 col-form-label">Email</label>
			<div class="col-sm-10">
			  <input type="email" class="form-control" disabled readonly value="<?= $user['email'] ?>">
			</div>
		  </div>
		</form>
	  </div>
	</div>
</main>
<!-- Add Movie Modal -->
<div class="modal fade" id="changePassword" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="#" id="addForm" class="form-horizontal">
		  <div class="modal-header">
			<h5 class="modal-title" id="staticBackdropLabel">Change Password</h5>
			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		  </div>
		  <div class="modal-body">
			  <div class="row mb-3">
				<label class="col-sm-4 col-form-label">Old Password</label>
				<div class="col-sm-8">
				  <input type="password" class="form-control" name="oPwd" placeholder="Enter Old Password">
				  <input type="hidden" id="url" value="<?php echo base_url('User/changePwd')?>">
				</div>
			  </div>
			  <div class="row mb-3">
				<label class="col-sm-4 col-form-label">New Password</label>
				<div class="col-sm-8">
				  <input type="password" class="form-control" name="nPwd" placeholder="Enter New Password">
				</div>
			  </div>
			  <div class="row mb-3">
				<label class="col-sm-4 col-form-label">Confirm Password</label>
				<div class="col-sm-8">
				  <input type="password" class="form-control" name="cPwd" placeholder="Confirm New Password">
				</div>
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
			<button type="submit" class="btn btn-primary">Change Password</button>
		  </div>
	  </form>
    </div>
  </div>
</div>