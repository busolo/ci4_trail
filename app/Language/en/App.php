<?php

return [
    'page_title' => 'Manage Movie Trailers',
    'sign_in' => 'Sign In',
    'email_address' => 'Email Address',
    'password' => 'Password',
    'please' => 'Please',
];
