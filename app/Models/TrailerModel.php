<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class TrailerModel extends Model {
    protected $DBGroup              = 'default';
	protected $table                = 'tbl_trailers';
	protected $primaryKey           = 'trailer_id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		"name",
		"url",
		"order",
		"deleted"
	];
	
	public function getTrailers($id = false) {
      if($id === false) {
        return $this->where('deleted', 0)->orderBy('order', 'ASC')->findAll();
      } else {
          return $this->find(['trailer_id' => $id]);
      }
    }
}