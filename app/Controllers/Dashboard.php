<?php 
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\TrailerModel;

class Dashboard extends Controller {
    
	public function __construct() {
		$this->model = new TrailerModel();
		$this->user_id = session()->get('user_id');
    }
	
    // show login form
    public function index() {
		$data['page_name'] = "Dashboard";
		$data['trailers'] = $this->model->getTrailers();
		echo view('includes/header', $data);
		echo view('includes/header-pages');
		echo view('includes/footer');
		echo view('dashboard');
    }	
	
	function addTrailer() {
		//print_r($this->request->getPost()); die();
		$type = $this->request->getPost('type');
		$id = $this->request->getPost('trailer_id');
		//View Trailer
		if($type == "Add") {
			echo view('trailers/addTrailer');
		} else if($type == "View") {
			$data['trailerData'] = $this->model->getTrailers($id);
			echo view('trailers/viewTrailer', $data);
		} else if($type == "Edit") {
			$data['trailerData'] = $this->model->getTrailers($id);
			echo view('trailers/editTrailer', $data);
		} else if($type == "Delete") {
			$dataSave = [
				'deleted' => 1
			];
			$this->model->update($id, $dataSave);
			echo "Trailer has been successfully deleted";
		} else {
			echo "Sorry we cannot process your request right now";
		}
	}
	
	function editTrailer() {
		//print_r($this->request->getPost()); die();
		$type = $this->request->getPost('type');
		$id = $this->request->getPost('trailerID');
		//Edit and Add Trailer Data
		if($type == "Edit") {
			$dataSave = [
				'name' => $this->request->getPost('name'),
				'order'    => $this->request->getPost('order'),
				'url'    => $this->request->getPost('url'),
			];
			$this->model->update($id, $dataSave);
			$resp = json_encode(['success'=> 'success', 'csrf' => csrf_hash(), 'msg' => "Trailer has been successfully updated"]);
		} else if($type == "Add") {
			$dataSave = [
				'name' => $this->request->getPost('name'),
				'order'    => $this->request->getPost('order'),
				'url'    => $this->request->getPost('url'),
			];
			$this->model->insert($dataSave);
			$resp = json_encode(['success'=> 'success', 'csrf' => csrf_hash(), 'msg' => "Trailer has been successfully added"]);
		} else {
			$resp = json_encode(['success'=> 'fail', 'csrf' => csrf_hash(), 'msg' => "Sorry we cannot process your request right now"]);
		}
		echo ($resp);
	}
}