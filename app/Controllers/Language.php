<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Language extends Controller
{
    public function index()
    {
        $session = session();
        $locale = $this->request->getLocale();
        $session->remove('lang');
        $session->set('lang', $locale);
        $url = base_url();
        return redirect()->back();
    }
	
	public function set( string $locale )
    {
        // Check requested language exist in \Config\App
        if ( in_array($locale, config('App')->supportedLocales) )
        {
            // Save requested locale in session, will be set by filter
            session()->set('locale', $locale);

            // Reload page
            return redirect()->back();
        }
        else
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException( esc($locale) ." is not a supported language" );
        }
    }
}
