<?php 
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\UserModel;

class Login extends Controller {
    
    // show login form
    public function index()    {  

        $session = session();  
		$data['page_name'] = "Sign In";
		echo view('includes/header', $data);
		$data = [];
        if ($this->request->getMethod() == 'post') {
			echo "dead";
			$model = new UserModel();
			$email = $this->request->getVar('email');
			$password = $this->request->getVar('password');
			$dataUser = $model->where('email', $email)->first();
			if($dataUser) {
				$pass = $dataUser['password'];
				$verify_pass = password_verify($password, $pass);
				if($verify_pass) {
					$ses_data = [
						'user_id'       => $dataUser['user_id'],
						'name'    		=> $dataUser['name'],
						'email'    		=> $dataUser['email'],
						'logged_in'     => TRUE
					];
					$session->set($ses_data);
					return redirect()->to('/Dashboard');
				} else {
					$session->setFlashdata('msg', 'Invalid Username or Password');
				}
			} else {
				$session->setFlashdata('msg', 'Invalid Username or Password');
			}
        } else {
			$session->setFlashdata('msg', '');
		}
        echo view('login');
		echo view('includes/footer');
    }      
	
    private function setUserSession($user) {
        $data = [
            'user_id' => $user['user_id'],
            'name' => $user['name'],
            'email' => $user['email']
        ];

        session()->set($data);
        return true;
    }
	 
    //signout function
    public function Logout() {
        $session = session();  
        $session->destroy();
        $session->setFlashdata('msg', '');
        return redirect()->to('/');
     }
}