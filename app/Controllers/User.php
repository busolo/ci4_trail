<?php 
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\UserModel;

class User extends Controller {
	
	public function __construct() {
		$this->model = new UserModel();
		$this->user_id = session()->get('user_id');
    }
	
    
    // show login form
    public function index()    {
		$data['page_name'] = "My Account";
		$data['user'] = $this->model->find($this->user_id);
		echo view('includes/header', $data);
		echo view('includes/header-pages');
		echo view('my_account');
		echo view('includes/footer');
    }
	
	function changePwd() {
		//Change Password
		helper(['form', 'url']);
		$old_pwd = $this->request->getPost('oPwd');
		$new_pwd = $this->request->getPost('nPwd');
		$c_pwd = $this->request->getPost('cPwd');
		
		if($new_pwd === $c_pwd && ($new_pwd != "" || $c_pwd != "")) {
			$user = $this->model->find($this->user_id);
			if($user) {
				$pass = $user['password'];
				$verify_pass = password_verify($old_pwd, $pass);
				if($verify_pass) {
					$data = array(
						'password' => password_hash($new_pwd, PASSWORD_DEFAULT)
					);
					$insert = $this->model->update($this->user_id, $data);
					$resp = json_encode(['success'=> 'success', 'csrf' => csrf_hash(), 'msg' => "Password has been successfully changed"]);
				} else {
					$resp = json_encode(['success'=> 'fail', 'csrf' => csrf_hash(), 'msg' => "Sorry password not changed, Invalid Old Password"]);
				}
			} else {
				$resp = json_encode(['success'=> 'fail', 'csrf' => csrf_hash(), 'msg' => "Sorry Password not changed, User cannot be found"]);			
			}
		} else {
			$resp = json_encode(['success'=> 'fail', 'csrf' => csrf_hash(), 'msg' => "Sorry Password not changed, New Password and Confirm Password do not match"]);			
		}
		return $resp;
        /* $data = array(
            'password' => $this->request->getPost('pwd'),
            'url' => $this->request->getPost('url')
        );
        $insert = $this->model->book_add($data);
        echo json_encode(array("status" => TRUE)); */
	}
}