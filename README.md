# CodeIgniter 4 Movie Trailer App

## Installation Guide

- Open and Edit app/Config/App.php with base_url
- Open and edit app/Config/Database.php with DB configuration. 
- Sample DB is public folder under db

**Please** read the user guide for a better explanation of how the system works!


## Access
- sam@me.com, Password - sam
- admin@me.com, Password - admin

## Server Requirements
- PHP 8.0.3
- Codeigniter 4.1.1
- 10.4.18-MariaDB
- OOP Standards on Zend Engine v4.0.3
- Bootstrap v5.0

