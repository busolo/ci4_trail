-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.18-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for fao
DROP DATABASE IF EXISTS `fao`;
CREATE DATABASE IF NOT EXISTS `fao` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `fao`;

-- Dumping structure for table fao.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table fao.migrations: ~0 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table fao.tbl_trailers
DROP TABLE IF EXISTS `tbl_trailers`;
CREATE TABLE IF NOT EXISTS `tbl_trailers` (
  `trailer_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `order` bigint(255) NOT NULL DEFAULT 0,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`trailer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table fao.tbl_trailers: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_trailers` DISABLE KEYS */;
REPLACE INTO `tbl_trailers` (`trailer_id`, `deleted`, `order`, `name`, `url`) VALUES
	(14, 1, 4, 'Moana Trailer', 'https://www.youtube.com/embed/LKFuXETZUsI'),
	(15, 0, 4, 'Lion King Trailer 2019', 'https://www.youtube.com/embed/7TavVZMewpY'),
	(16, 0, 1, 'Mowgli Official Trailer', 'https://www.youtube.com/embed/OVBjPpUlQrE');
/*!40000 ALTER TABLE `tbl_trailers` ENABLE KEYS */;

-- Dumping structure for table fao.tbl_users
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table fao.tbl_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
REPLACE INTO `tbl_users` (`user_id`, `name`, `email`, `password`) VALUES
	(1, 'Samuel One', 'sam@me.com', '$2y$10$aMiISwRobOVm.jGaAcpofeMvLriLYS8yJQ/lAetKaNlQurBax/J.K'),
	(2, 'Admin Two', 'admin@me.com', '$2y$10$t.qEw480rCstvW7g22fCW.Z3u19QDk/zxLGjHow5gGOCAokDbUpLK');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
