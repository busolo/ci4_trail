	$(document).ready(function(){
		$('#addForm').submit(function(e) {
        e.preventDefault();
		var site_url = $("#url").val();
        $.ajax({
			url : site_url,
            type: "POST",
            data: $('#addForm').serialize(),
            dataType: "JSON",
            success: function(data) {
				//Get Results
				//Check success
				if(data.success == "success") {
				   $('#addForm')[0].reset();
				   $('#changePassword').modal('hide');
				   alert(data.msg);					
				} else {
				   alert(data.msg);					
				}
           },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error submitting your data, please try again');
            }
       });
     });
});